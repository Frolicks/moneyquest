﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public GameObject[] buttons; 
    public GameObject cursor;

    private int btnIndex;
	// Use this for initialization
	void Start()
    {
        btnIndex = 0; 

    }
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }
        cursor.transform.position = buttons[btnIndex].transform.position + Vector3.up * 2.8f + Vector3.left * 1.7f + Vector3.back * 2f;

        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (btnIndex == buttons.Length - 1)
                btnIndex = 0;
            else
                btnIndex++;
            GetComponent<AudioSource>().Play(); 
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (btnIndex == 0)
                btnIndex = buttons.Length - 1;
            else
                btnIndex--;
            GetComponent<AudioSource>().Play();
        }

        if(Input.GetKeyDown(KeyCode.E))
        {
            if (btnIndex == 0)
            {
                SceneManager.LoadScene("Help");
            }
            if (btnIndex == 1)
            {
                startGame();
            }
            if (btnIndex == 2)
            {
                SceneManager.LoadScene("Leaderboard"); 
            }
        }
	}
    void startGame()
    {
        //gives global variables default values
        DATA.health = 3f;
        DATA.lives = 3f;
        DATA.score = 0f;
        DATA.money = 0f;
        DATA.spawnedCanvas = false;
        SceneManager.LoadScene("Level1"); 

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FinalScore : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Text>().text = "FINAL SCORE: " + DATA.score; 
	}
	
	// Update is called once per frame
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class DeathHandling : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Time.timeScale = 0f; 
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.E))
        {
            if (DATA.lives > 0)
            {
                DATA.health = 3;
                DATA.lives--;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
                SceneManager.LoadScene("LBInput");
        }
		
	}
    private void OnDestroy()
    {
        Time.timeScale = 1f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCanvas : MonoBehaviour {

    // Use this for initialization
    private void OnDestroy()
    {
        Destroy(GameObject.Find("Canvas"));
    }
}

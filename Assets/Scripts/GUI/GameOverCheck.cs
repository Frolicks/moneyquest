﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverCheck : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(DATA.lives <= 0f)
        {
            GetComponent<TextMesh>().text = "GAME OVER"; 
        } else
        {
            GetComponent<TextMesh>().text = "You've died!";
        }
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class GuiNavigation : MonoBehaviour {
    public string sceneOnE;
    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.E))
        {
            SceneManager.LoadScene(sceneOnE);
        }
        if (Input.GetKey("escape"))
        {
            Application.Quit(); 
        }
    }
}

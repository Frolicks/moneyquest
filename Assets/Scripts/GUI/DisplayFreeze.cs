﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayFreeze : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Time.timeScale = 1f;
            Destroy(gameObject); 
        }

        
    }
}

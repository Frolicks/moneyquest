﻿using UnityEngine;
using UnityEngine.UI; 

public class LeaderboardDisplay : MonoBehaviour
{

    private Text board; 
    private void Start()
    {
        board = GetComponent<Text>(); 
        
        for (int i = 0; i < Leaderboard.EntryCount; ++i)
        {
            var entry = Leaderboard.GetEntry(i);
            board.text += entry.name + " - " + entry.score + "\n";
        }
    }
}
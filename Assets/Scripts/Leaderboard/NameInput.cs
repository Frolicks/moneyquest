﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NameInput : MonoBehaviour {

   public string pName;

    public GameObject theCanvas;

   public int x, y, width, height; 

    public GUIStyle pixely;

    private int count; 
    private void OnGUI()
    {
        GUI.SetNextControlName("NameInput");
        pName = GUI.TextField(ResizeGUI(new Rect(x, y, width, height)), pName, 20, pixely);
        GUI.FocusControl("NameInput");

        if (pName.Length == 20)
        {
            SceneManager.LoadScene("Leaderboard");
        }
    }

    private void OnDestroy()
    {
        Leaderboard.Record(pName, (int)DATA.score);
    }

    Rect ResizeGUI(Rect _rect)
    {
        float FilScreenWidth = _rect.width / 800;
        float rectWidth = FilScreenWidth * Screen.width;
        float FilScreenHeight = _rect.height / 600;
        float rectHeight = FilScreenHeight * Screen.height;
        float rectX = (_rect.x / 800) * Screen.width;
        float rectY = (_rect.y / 600) * Screen.height;

        return new Rect(rectX, rectY, rectWidth, rectHeight);
    }
}

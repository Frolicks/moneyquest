﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTimer : MonoBehaviour {
    public float lifespan; 

    private float lifetime;
    // Update is called once per frame
    private void Start()
    {
        lifetime = 0f;
    }
 
	void Update () {
        lifetime += Time.deltaTime;
        if (lifetime >= lifespan)
            Destroy(gameObject);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class QuestionScript : MonoBehaviour {
    public GameObject prompt, cursor;
    public GameObject[] answers;
    public int answerDex;

    private bool answered; 
    private int aIndex, ePresses;

    public string textPrompt;
    public string[] tAnswers = new string[3]; 
    public int theAnswer; 

    private AudioSource[] ass; 
	// Use this for initialization
	void Start () {
        ass = transform.root.GetComponents<AudioSource>();
        GameObject.Find("player").GetComponent<Rigidbody2D>().velocity = Vector3.zero;
	}
    void OnEnable()
    {
        cursor.SetActive(true); 
        Time.timeScale = 0f;
        aIndex = 0;

        ePresses = 0;
        ass = transform.root.GetComponents<AudioSource>();
        prompt.GetComponent<Text>().text = textPrompt;
        for (int i = 0; i < 3; i++)
        {
            answers[i].GetComponent<Text>().text = tAnswers[i]; 
        }
        answerDex = theAnswer;
        GetComponent<RectTransform>().localScale = new Vector3(0.75f, 0.75f, 1f); 
    }
    // Update is called once per frame
	
	// Update is called once per frame
	void Update () {

        cursor.GetComponent<RectTransform>().anchoredPosition = answers[aIndex].GetComponent<RectTransform>().anchoredPosition; //+ Vector3.up * 2.8f + Vector3.left * 1.7f + Vector3.back * 2f;

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Debug.Log("test");
            if (aIndex == answers.Length - 1)
                aIndex = 0;
            else
                aIndex++;
            ass[1].Play();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (aIndex == 0)
                aIndex = answers.Length - 1;
            else
                aIndex--;
            ass[1].Play();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (aIndex == answerDex && ePresses == 0)
            {
                if(DATA.health < 5) {
                    prompt.GetComponent<Text>().text = "Correct! +1 HP!";
                    DATA.health++;
                    ass[2].Play();
                } else
                {
                    prompt.GetComponent<Text>().text = "Correct!";
                    ass[2].Play();
                }

            }
            else
            {
                prompt.GetComponent<Text>().text = "Incorrect";
            }
            ePresses++; 
            cursor.SetActive(false);
            if (ePresses > 1)
            {
                gameObject.SetActive(false); 
            }
            ass[1].Play();
        }

    }
    void OnDisable()
    {
        GameObject.Find("player").GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        Time.timeScale = 1f;
    }
}

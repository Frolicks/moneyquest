﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuestionDirectory : MonoBehaviour {
    public GameObject[] question = new GameObject[20];
    public GameObject[] taxes = new GameObject[3]; 

    private int qDex, tDex; 

    // Use this for initialization
    void Start () {
        if(!DATA.spawnedCanvas)
        {
            DontDestroyOnLoad(transform.gameObject);
            DATA.spawnedCanvas = true; 
        } else if (SceneManager.GetActiveScene().name == "Level1")
        {
            Destroy(gameObject); 
        }

	}
	
	// Update is called once per frame
	void Update () {
        if(SceneManager.GetActiveScene().name == "Menu")
        {
            Destroy(gameObject); 
        }
	}

    public void taxMe()
    {
        taxes[(int)Random.Range(0, 3 - 0.1f)].SetActive(true);   
    }

    public void askQuestion()
    {
        question[(int)Random.Range(0, 20 - 0.1f)].SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class TaxScript : MonoBehaviour {
    public GameObject prompt, cursor;
    public GameObject[] answers;
    public GameObject monster; 
    public int answerDex;

    private bool answered; 
    private int aIndex, ePresses;

    public string textPrompt;
    public string[] tAnswers = new string[3]; 
    public int theAnswer;

    private bool gonnaTurn;

    private AudioSource[] ass;

    private float fif, ten, twen;
	// Use this for initialization
    void OnEnable()
    {
        ass = transform.root.GetComponents<AudioSource>();
        Time.timeScale = 0f;
        aIndex = 0;

        gonnaTurn = false;
        ePresses = 0;
        fif = DATA.money * 0.15f;
        ten = DATA.money * 0.10f;
        twen = DATA.money * 0.20f; 
        ass = transform.root.GetComponents<AudioSource>();
        prompt.GetComponent<Text>().text = textPrompt;
        answers[0].GetComponent<Text>().text = "" + twen.ToString("F2");
        answers[1].GetComponent<Text>().text = "" + ten.ToString("F2");
        answers[2].GetComponent<Text>().text = "" + fif.ToString("F2");
        answerDex = theAnswer; 
    }
	
	// Update is called once per frame
	void Update () {

        cursor.GetComponent<RectTransform>().anchoredPosition = answers[aIndex].GetComponent<RectTransform>().anchoredPosition; //+ Vector3.up * 2.8f + Vector3.left * 1.7f + Vector3.back * 2f;

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (aIndex == answers.Length - 1)
                aIndex = 0;
            else
                aIndex++;
            ass[1].Play();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (aIndex == 0)
                aIndex = answers.Length - 1;
            else
                aIndex--;
            ass[1].Play();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (aIndex == answerDex)
            {
                prompt.GetComponent<Text>().text = "You may pass!";
                DATA.health++;
                ass[2].Play(); 
            }
            else
            {
                prompt.GetComponent<Text>().text = "Wrong! Prepare yourself!";
                gonnaTurn = true; 
            }
            ass[1].Play();
            ePresses++; 
            cursor.SetActive(false);
            if (ePresses > 1)
            {
                gameObject.SetActive(false); 
            }
        }

    }
    void OnDisable()
    {
        if(gonnaTurn)
        {
            Instantiate(monster, GameObject.Find("Gatekeeper").transform.position, Quaternion.identity);
        }
        if (answerDex == 0) DATA.money -= twen;
        if (answerDex == 1) DATA.money -= ten;
        if (answerDex == 2) DATA.money -= fif;
        GameObject.Find("player").GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        Time.timeScale = 1f;
    }
}

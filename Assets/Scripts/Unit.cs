﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unit : MonoBehaviour {
    public float health, dealtDamage, aggroRange;
    [HideInInspector]
    protected Vector3 vToPlayer;
    [HideInInspector]
    protected float dToPlayer; 
    public GameObject flash;
    public GameObject dmgPop, player;

    public GameObject Coin; 
    [HideInInspector]
    protected Rigidbody2D rb;
    [HideInInspector]
    protected BoxCollider2D bc;
    [HideInInspector]
    protected SpriteRenderer sr;
    [HideInInspector]
    protected Animator an;
    [HideInInspector]
    protected bool aggroed; 

	// Use this for initialization
	public virtual void Start () {
        aggroed = false; 
        player = GameObject.Find("player");
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();
        an = GetComponent<Animator>();
    }

    public virtual void Update()
    {
        dToPlayer = Vector3.Distance(this.transform.position, player.transform.position);
        vToPlayer = (player.transform.position - transform.position).normalized;
        if (health <= 0)
            Destroy(gameObject);

        if(dToPlayer < aggroRange)
        {
            aggroed = true; 
        }
    }

    public void coolDown(ref float abilityTimer)
    {
        if (abilityTimer > 0)
            abilityTimer -= Time.deltaTime;
    }

    public void getDamaged(float dmg, float knockBackForce, Vector3 origin)
    {
        aggroed = true; 
        health -= dmg; 
        rb.AddForce(-vToPlayer * knockBackForce, ForceMode2D.Impulse);
        //Instantiate(flash, new Vector3(transform.position.x, transform.position.y, 0f), Quaternion.identity);
        GameObject spawnedDmgPop = (GameObject)Instantiate(dmgPop, new Vector3(transform.position.x, transform.position.y, -1f), Quaternion.identity);
        spawnedDmgPop.GetComponent<TextMesh>().text = dmg.ToString();
        if (CompareTag("Enemy"))  
            spawnedDmgPop.GetComponent<TextMesh>().color = Color.yellow;
        else
            spawnedDmgPop.GetComponent<TextMesh>().color = Color.red; 
        spawnedDmgPop.GetComponent<Rigidbody2D>().AddForce(Vector3.up * 6f, ForceMode2D.Impulse); 
    }

    public virtual void OnDestroy()
    {
        Instantiate(Coin, transform.position, Quaternion.identity); 
    }

}

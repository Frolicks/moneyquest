﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSpawning : MonoBehaviour {

    public Sprite[] trees;

    public float scaleX, scaleY;

    private SpriteRenderer sr; 
	
	// Update is called once per frame
	void Start () {
        sr = GetComponent<SpriteRenderer>();
        sr.sprite = trees[(int)Random.Range(0f, trees.Length - 0.1f)];
        transform.localScale = new Vector3(scaleX, scaleY, 1f);
        sr.sortingOrder = (int)transform.TransformPoint(transform.position).y * -1;
	}
}

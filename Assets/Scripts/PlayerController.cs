﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class PlayerController : Unit
{
    public float speed, slashingSpeed, maxSpeed, dodgeVelocity, shootForce;
    public float dodgeCD, shootCD, slashCD, hurtCD;
    public GameObject rock, sword, mCamera, DeathScreen;

    public Text healthText, moneyText, scoreText, livesText;

    private float shootTimer, slashTimer, dodgeTimer, hurtTimer;
    private bool melee, vulnerable;

    private Vector2 movement, attackDirection;

    private GameObject spawnedSword;
    private QuestionDirectory qd; 
    //camera-related
    public float freeCamMagnitude;
    private Vector3 dampVelocity = Vector3.zero;

    private AudioSource ass; 

    public override void Start()
    {
        ass = GetComponent<AudioSource>(); 
        qd = GameObject.Find("Canvas").gameObject.GetComponent<QuestionDirectory>(); 
        base.Start();
        dodgeTimer = 0f;
        moneyText = GameObject.Find("Money").GetComponent<Text>();
        healthText = GameObject.Find("Health").GetComponent<Text>();
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        livesText = GameObject.Find("Lives").GetComponent<Text>();
        health = 99; // this value isn't used
    }

    // Update is called once per frame
    public override void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }
        base.Update();
        movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        attackDirection = new Vector2(Input.GetAxis("ArrowHorizontal"), Input.GetAxis("ArrowVertical"));

        if (Input.GetKey(KeyCode.E))
        {
            melee = true;
        }
        else
            melee = false; 
        //MOVEMENT CONTROLS
        //dash
        /*if (Input.GetKeyDown(KeyCode.Space) && dodgeTimer <= 0f)
        {
            rb.velocity = (movement * dodgeVelocity);
            dodgeTimer = dodgeCD;
        } scrapped doging ability due to clipping issues */ 
        if (movement.magnitude > maxSpeed)
        {
            rb.velocity *= 0.5f;
        }

        //COOLDOWNS
        coolDown(ref dodgeTimer);
        coolDown(ref slashTimer);
        coolDown(ref shootTimer);
        coolDown(ref hurtTimer);

        //ATTACKING
        if (attackDirection.magnitude > 0f)
        {
            if (shootTimer <= 0 && !melee)
            {
                dealtDamage = 1f;
                GameObject spawnedRock = (GameObject)Instantiate(rock, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                spawnedRock.GetComponent<Rigidbody2D>().AddForce(new Vector2(attackDirection.x + Random.Range(-0.05f, 0.05f), attackDirection.y + Random.Range(-0.05f, 0.05f)) * shootForce);
                shootTimer = shootCD + Random.Range(-0.1f, 0.1f);
            }
            if (slashTimer <= 0 && melee)
            {
                dealtDamage = 3f;
                spawnedSword = (GameObject)Instantiate(sword, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                Vector3 diff = attackDirection;
                float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
                spawnedSword.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90); //-90 to adjust spawn
                spawnedSword.transform.Rotate(new Vector3(0f, 0f, 45f));
                spawnedSword.GetComponent<Rigidbody2D>().centerOfMass = Vector2.down * 0.05f;
                spawnedSword.GetComponent<Rigidbody2D>().AddTorque(-700f);
                slashTimer = slashCD;
            }

        }

        if (slashTimer <= 0)
            Destroy(spawnedSword);

        if (hurtTimer > 0)
        {
            sr.material.color = new Color(1f, 1f, 1f, 0.25f);
        }
        else
            sr.material.color = new Color(1f, 1f, 1f, 1f);

        if (dodgeTimer > 0)
        {
            bc.isTrigger = true;
        }
        else
            bc.isTrigger = false;

        vulnerable = (dodgeTimer <= 0f && hurtTimer <= 0f);

        //GUI
        healthText.text = "HP: " + DATA.health;
        moneyText.text = "$: " + DATA.money.ToString("F2");
        livesText.text = "Lives: " + DATA.lives;
        scoreText.text = "SCORE: " + DATA.score; 
        

        //ANIMATIONS
        animate();

        //DYING
        if (DATA.health <= 0 && GameObject.Find("DeathScreen(Clone)") == null) 
        {
            Instantiate(DeathScreen, new Vector3(mCamera.transform.position.x, mCamera.transform.position.y, 2f), Quaternion.identity); 
        }
        
    }

    private void FixedUpdate()
    {
        if (spawnedSword == null && dodgeTimer <= 0.25)
        {
            rb.AddForce(movement * speed);
            if (rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity *= 0.5f;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EProjectile")) //assuming its a projectile
        {
            if (vulnerable)
            {
                getDamaged(1f, 0f, other.transform.position); // projectiles only do 1 dmg? 
                DATA.health--;
                hurtTimer = hurtCD;
            }
        }
        if(other.CompareTag("Mentor"))
        {
            qd.askQuestion();
        }
        if (other.CompareTag("GateKeeper"))
        {
            qd.taxMe(); 
        }
        if (other.CompareTag("Coin"))
        {
            if(other.GetComponent<Coin>().coinDex == 0)
            {
                DATA.money += 0.01f;
                Destroy(other.gameObject); 
            }
            if(other.GetComponent<Coin>().coinDex == 1)
            {
                DATA.money += 0.1f;
                Destroy(other.gameObject);
            }
            if(other.GetComponent<Coin>().coinDex == 2)
            {
                DATA.money += 1f;
                Destroy(other.gameObject);
            }
            ass.Play(); 
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (vulnerable)
            {
                getDamaged(collision.gameObject.GetComponent<Unit>().dealtDamage, 0f, collision.transform.position);
                DATA.health--; 
                hurtTimer = hurtCD;
            }
        }
    }

    //CAMERA ROOM-PAN
    private void OnTriggerStay2D(Collider2D collider)
    {
        Vector3 roomCenter = collider.transform.position;
        if (collider.CompareTag("Room")) {
            if(collider.GetComponent<BoxCollider2D>().size.magnitude < 5f)
            {
                mCamera.transform.position = Vector3.SmoothDamp(mCamera.transform.position, roomCenter + Vector3.forward * -10f, ref dampVelocity, 0.4f);
            }
            else
            {
                mCamera.transform.position = Vector3.SmoothDamp(mCamera.transform.position, transform.position + Vector3.forward * -10f, ref dampVelocity, 0.2f);
            }
        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Mentor") || collision.CompareTag("GateKeeper"))
            Destroy(collision.gameObject);
    }

    private void animate()
    {
        if (movement.magnitude > 0f)
        {
            if (movement + attackDirection == Vector2.zero)
                an.SetFloat("Playback", -0.5f);
            else
                an.SetFloat("Playback", 0.75f);
        }
        else
            an.SetFloat("Playback", 0f);

        if (attackDirection.magnitude > 0f)
        {
            if (attackDirection == Vector2.up)
            {
                an.Play("up");
                sr.sortingOrder = 2;
            }
            else if (attackDirection == -Vector2.up)
            {
                an.Play("down");
                sr.sortingOrder = 0;
            }
            else if (attackDirection == Vector2.left)
            {
                an.Play("left");
                sr.sortingOrder = 0;
            }
            else if (attackDirection == -Vector2.left)
            {
                an.Play("right");
                sr.sortingOrder = 0;
            }
        }
        else
        {
            if (movement == Vector2.up)
            {
                an.Play("up");
                sr.sortingOrder = 2;
            }
            else if (movement == -Vector2.up)
            {
                an.Play("down");
                sr.sortingOrder = 0;
            }
            else if (movement == Vector2.left)
            {
                an.Play("left");
                sr.sortingOrder = 0;
            }
            else if (movement == -Vector2.left)
            {
                an.Play("right");
                sr.sortingOrder = 0;
            }
        }
    }
}

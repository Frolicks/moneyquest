﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
    public GameObject[] coins = new GameObject[3];
    public int coinDex;

    private GameObject spawnedCoin;
	// Use this for initialization
	void Start () {
        coinDex = (int)Random.Range(0, 3 - 0.1f);
        spawnedCoin = (GameObject) Instantiate(coins[coinDex], transform.position, Quaternion.identity);
        spawnedCoin.transform.parent = transform;
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DATA : object {
    public static float health;
    public static float lives;
    public static float money;
    public static float score;

    public static bool spawnedCanvas;
}

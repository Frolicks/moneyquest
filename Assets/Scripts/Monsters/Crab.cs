﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crab : Unit {
    public float speed, shootCD;

    public GameObject los, crabShot;

    private Vector2 sightline; 
    private float shootTimer;

    private RaycastHit2D seen, saw;
    // Use this for initialization
    public override void Start () {
        base.Start();
        transform.Rotate((int)Random.Range(1f,4f) * 90 * Vector3.forward); 
	}
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
        if (aggroed)
        {
            rb.velocity = transform.right * speed;
            sightline = los.transform.position - transform.position;

            if (Random.value < 0.01f)
            {
                transform.Rotate((int)Random.Range(1f, 4f) * 90 * Vector3.forward);
            }

            seen = Physics2D.Raycast(transform.position, sightline);
            if (seen && seen.collider.gameObject.CompareTag("Player") && shootTimer <= 0f)
            {
                saw = seen;
                Invoke("shoot", 0f);
                Invoke("shoot", 0.5f);
                shootTimer = shootCD;
            }
        }

        coolDown(ref shootTimer);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Weapon"))
        {
            Instantiate(flash, new Vector3(other.transform.position.x, other.transform.position.y), Quaternion.identity);
            getDamaged(player.GetComponent<PlayerController>().dealtDamage, 0f, this.transform.position);
            if (other.gameObject.GetComponent<Rigidbody2D>().angularVelocity == 0f) // cheesy way to determine if rock or sword ; rocks don't have angular velocity amirite
                other.GetComponent<DestroyTimer>().lifespan = 0; //cheesy way to destroy an object;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        transform.Rotate((int)Random.Range(1f, 4f) * 90 * Vector3.forward);
    }

    void shoot()
    {
        GameObject spawnedShot = (GameObject)Instantiate(crabShot, transform.position, Quaternion.identity);
        spawnedShot.transform.up = sightline.normalized;
        spawnedShot.GetComponent<Rigidbody2D>().AddForce((saw.transform.position - transform.position).normalized * 500f); 
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        if(health == 0)
            DATA.score += 125f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boar : Unit {
    public float chargeCD;
    private float chargeTimer; 
	// Use this for initialization
	public override void Start () {
        base.Start();
        chargeTimer = chargeCD; 
	}
    public GameObject snort, dust; 

	// Update is called once per frame
	public override void Update () {
        base.Update();
        if (aggroed) 
        {
            if(chargeTimer > 0f)
            {
                dust.SetActive(false);
                snort.SetActive(true);
                transform.up = -(player.transform.position - transform.position).normalized;
            }
            if (chargeTimer <= 0f)
            {
                snort.SetActive(false);
                dust.SetActive(true);    
                rb.AddForce(vToPlayer * 150f);
            }
            coolDown(ref chargeTimer);
        }
        
 
		
	}
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (dust.activeSelf)
        {
            chargeTimer = chargeCD;
            dust.SetActive(false);
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Weapon"))
        {
            Instantiate(flash, new Vector3(other.transform.position.x, other.transform.position.y), Quaternion.identity);
            getDamaged(player.GetComponent<PlayerController>().dealtDamage, 1f, this.transform.position);
            if (other.gameObject.GetComponent<Rigidbody2D>().angularVelocity == 0f) // cheesy way to determine if rock or sword ; rocks don't have angular velocity amirite
                other.gameObject.GetComponent<DestroyTimer>().lifespan = 0; //cheesy way to destroy an object 
        }
    }
    public override void OnDestroy()
    {
        base.OnDestroy();
        if (health == 0)
            DATA.score += 150f; 
    }
}

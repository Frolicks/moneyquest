﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : Unit {

    public float dashCD, speed;

    private float damagedTimer;
    
	// Use this for initialization
	public override void Start () {
        base.Start(); 
	}
	
	// Update is called once per frame
	public override void Update () {
        base.Update(); 
        if (aggroed)
        {
            transform.up = -vToPlayer;
            if (damagedTimer <= 0f)
            {
                rb.velocity = vToPlayer * speed;
            }
            else
                rb.velocity = vToPlayer * -(speed / 1.01f);
        }
        coolDown(ref damagedTimer); 
        
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Weapon"))
        {
            Instantiate(flash, new Vector3(other.transform.position.x, other.transform.position.y), Quaternion.identity);
            getDamaged(player.GetComponent<PlayerController>().dealtDamage, 0f, this.transform.position);
            if (other.gameObject.GetComponent<Rigidbody2D>().angularVelocity == 0f) // cheesy way to determine if rock or sword ; rocks don't have angular velocity amirite
                other.GetComponent<DestroyTimer>().lifespan = 0; //cheesy way to destroy an object;
            damagedTimer = 0.005f; 
            
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
            an.SetBool("Stabbing", true);
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
            an.SetBool("Stabbing", false);
    }
    public override void OnDestroy()
    {
        base.OnDestroy();
        if(health == 0)
            DATA.score += 100f; 
    }
}

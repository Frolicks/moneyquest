﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : Unit {
    public GameObject arrow;
    public float shootingRange, shootCD, speed;

    private float shootTimer; 
	// Use this for initialization
	public override void Start () {
        base.Start();
        shootTimer = 0f; 
	}
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
        transform.up = -vToPlayer;
        if (aggroed)
        {
            an.speed = 1f;
            if (dToPlayer < shootingRange && shootTimer <= 0f)
            {
                rb.velocity = Vector2.zero; 
                for (float i = 1; i < 2; i += 0.5f)
                    Invoke("Shoot", i);
                shootTimer = shootCD * 2;
                an.Play("firing"); 
            }
            else if (shootTimer <= shootCD)
            {
                rb.velocity = vToPlayer * speed;
                an.Play("walking");
            }
        }
        else
        {
            an.speed = 0f; 
        }

        coolDown(ref shootTimer); 
	}

    private void Shoot()
    { 
        GameObject spawnedArrow = Instantiate(arrow, transform.position, Quaternion.identity);
        spawnedArrow.transform.up = vToPlayer;
        spawnedArrow.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * 300f);
        shootTimer = shootCD + 0.5f;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Weapon"))
        {
            Instantiate(flash, new Vector3(other.transform.position.x, other.transform.position.y), Quaternion.identity);
            getDamaged(player.GetComponent<PlayerController>().dealtDamage, 5f, this.transform.position);
            if (other.gameObject.GetComponent<Rigidbody2D>().angularVelocity == 0f) // cheesy way to determine if rock or sword ; rocks don't have angular velocity amirite
                other.GetComponent<DestroyTimer>().lifespan = 0; //cheesy way to destroy an object 
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        if(health == 0)
            DATA.score += 125f; 
    }
}

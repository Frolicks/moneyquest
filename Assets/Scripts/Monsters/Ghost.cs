﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : Unit {
    public GameObject ghostBullet; 
    public float moveForce, maxVelocity, knockBackForce, visibleCD, bulletForce, vulnerableTime, shootingRange;

    private bool aboutToAttack; 

    private float visibleTimer; // visible when negative

     public override void Start () {
        base.Start();
        visibleTimer = 0;
    }

    public override void Update () {
        base.Update();
        if (aggroed)
        {
            transform.up = -vToPlayer;
            rb.AddForce(vToPlayer * moveForce);

            if (rb.velocity.magnitude > maxVelocity)
                rb.velocity *= 0.5f;

            if (visibleTimer > 0f)  // make invisible
            {
                sr.material.color = new Color(1f, 1f, 1f, 0.25f);
                bc.isTrigger = true;
                aboutToAttack = false;
            }

            if (visibleTimer <= 0f && !aboutToAttack) // attack and stay visible for a while 
            {
                sr.material.color = new Color(1f, 1f, 1f, 1f);
                bc.isTrigger = false;
                if (dToPlayer < shootingRange)
                {
                    Invoke("popAttack", vulnerableTime);
                    aboutToAttack = true;
                }
            }

            coolDown(ref visibleTimer);
        }
            
    }

    void popAttack()
    {
        for(int deg = 12; deg <= 360; deg += 360/10)
        {
            GameObject spawnedGB = (GameObject)Instantiate(ghostBullet, new Vector2(transform.position.x, transform.position.y), Quaternion.Euler(new Vector3(0f, 0f, deg)));
            spawnedGB.GetComponent<Rigidbody2D>().AddRelativeForce(Vector3.up * bulletForce);
            sr.material.color = new Color(1f, 1f, 1f, 1f);
            visibleTimer = visibleCD;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Weapon") && visibleTimer <= 0f)
        {
            Instantiate(flash, new Vector3(other.transform.position.x, other.transform.position.y), Quaternion.identity);
            getDamaged(player.GetComponent<PlayerController>().dealtDamage, knockBackForce, this.transform.position);
            if (other.gameObject.GetComponent<Rigidbody2D>().angularVelocity == 0f) // cheesy way to determine if rock or sword ; rocks don't have angular velocity amirite
                other.GetComponent<DestroyTimer>().lifespan = 0; //cheesy way to destroy an object 
        }
    }
    public override void OnDestroy()
    {
        base.OnDestroy();
        if (health == 0)
            DATA.score += 200f; 
    }
}
